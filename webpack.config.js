const CopyWebpackPlugin = require('copy-webpack-plugin');
const HubSpotAutoUploadPlugin = require('@hubspot/webpack-cms-plugins/HubSpotAutoUploadPlugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const autoprefixer = require('autoprefixer');
// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack')
// const BrotliPlugin = require('brotli-webpack-plugin');

const hubspotConfig = ({ portal, autoupload } = {}) => {
  return {
    target: 'web',
    entry: {
      main: './src/index.js',
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
    },
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin({
          include: /vendor\.js$/,
        }),
      ],
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all'
          },
        },
      },
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          },
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { url: false } },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [autoprefixer()],
              },
            },
            'sass-loader',
          ],
        },
        {
          test: /\.(svg)$/,
          use: [
            {
              loader: 'url-loader',
            },
          ],
        },
        {
          test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
          loader: require.resolve('url-loader'),
          options: {
          limit: 10000,
          name: 'images/[name].[ext]' //[hash:8].[ext]'
          }
        },
        {
          test: /\.css$/,
          use:['style-loader','css-loader']
        },
        {
          test: /\.(woff|woff2|eot|ttf)$/,
          loader: 'url-loader' //?limit=100000'
        },
        // {
        //   test: /\.(ico|jpe?g|png|gif|webp|svg|mp4|webm|wav|mp3|m4a|aac|oga)(\?.*)?$/,
        //   loader: "file-loader"
        // }
        
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production'),
      }),
      // new BundleAnalyzerPlugin(),
      // new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new HubSpotAutoUploadPlugin({
        portal,
        autoupload,
        src: 'dist',
        // dest: 'The_Next_Street/react_modules/Scheduling',
        dest: 'The_Next_Street/react_modules/StudentPortal_Test/TNS_StudentPortal_Scheduling',
      }), 
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
      new CopyWebpackPlugin([
        { from: 'src/images', to: 'images' },
        {
          from: 'src/modules',
          to: 'modules',
        },
      ]),
    ],
  };
};
module.exports = [hubspotConfig];