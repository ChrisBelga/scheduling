import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import {Row, Col} from 'react-bootstrap'
import ScheduledRoadTestAccordion from '../ScheduleRoadTest/ScheduledRoadTestAccordion';
import RoadTestCalendar from './RoadTestCalendar';
import {DateRange, Search, Close} from '@material-ui/icons';
import RoadTestTable from './RoadTestTable';



const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1
    },
  }));


export default function ScheduleRoadTest() {

  const classes = useStyles();

    return(
        <Row>
            <Col xs={12}>
                <Row>
                    <Col className='accordionCol'>
                        <ScheduledRoadTestAccordion/>
                    </Col>
                </Row>
                <Row className='calendarRow' style={{paddingTop: '20px'}}>
                    <Col className='calendarCol' xs={12} md={5}>
                    <Row>
                        <Col>
                        <Card className='cardCalendar'>
                            <CardContent>
                            <Row>
                                <Col>
                                <Typography style={{color: '#4B77BE', fontWeight: '700', fontSize: '16px'}}>
                                    <DateRange/>FILTER BY DATE
                                </Typography>
                                </Col>
                            </Row>
                                <Col xs={12} className='ScheduleCalendarCol'>
                                    <RoadTestCalendar/>
                                </Col>
                            </CardContent>
                        </Card>
                        </Col>
                    </Row>
                    <Row>
                        <Col className='calendarColButton' xs={12} md={12}>
                        <Button
                            style={{backgroundColor: '#3598dc', borderRadius: '0px', marginRight: '7px'}}
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            startIcon={<Search />}
                        >
                            REFINE SEARCH
                        </Button>
                        <Button
                            className='calendarColcloseButton'
                            style={{backgroundColor: 'lightgray', borderRadius: '0px', color: 'black'}}
                            variant="contained"
                            color="secondary"
                            // className={classes.button}
                            startIcon={<Close style={{color: '#777'}} />}
                        >
                            CLEAR SEARCH
                        </Button>
                        </Col>
                    </Row>
                    </Col>
                    <Col className='cardCol' xs={12} md={7}>
                    <Card className='cardSlots'>
                        <CardContent>
                        <Row>
                            <Col>
                            <Row>
                                <Col xs={12} md={6} lg={12} className='headerTextCol'>
                                <Typography style={{color: '#4B77BE', fontWeight: '700', fontSize: '16px', display: 'inline'}}>
                                    <DateRange/>AVAILABLE OPEN SLOTS
                                </Typography>
                                </Col>
                                <Col className='openSlotButton' xs={12} md={6} lg={12}>
                                <Button
                                    style={{backgroundColor: '#3598dc', borderRadius: '0px', marginRight: '7px'}}
                                    variant="contained"
                                    color="secondary"
                                    className={classes.button}
                                    startIcon={<Search />}
                                >
                                    REFINE SEARCH
                                </Button>
                                <Button
                                    className='closeButton'
                                    style={{backgroundColor: 'lightgray', borderRadius: '0px', color: 'black'}}
                                    variant="contained"
                                    color="secondary"
                                    // className={classes.button}
                                    startIcon={<Close style={{color: '#777'}} />}
                                >
                                    CLEAR SEARCH
                                </Button>
                                </Col>
                            </Row>
                            </Col>
                        </Row>
                        <Row style={{marginTop: '30px'}}>
                            <Col>
                            <RoadTestTable/>
                            </Col>
                        </Row>
                        </CardContent>
                    </Card>
                    </Col>
                </Row>
            </Col>
      </Row>
    )
}