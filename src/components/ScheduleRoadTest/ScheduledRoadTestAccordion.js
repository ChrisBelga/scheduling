import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import DateRangeIcon from '@material-ui/icons/DateRange';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import Typography from '@material-ui/core/Typography'

const Accordion = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: '#555',
    color: 'white',
    padding: '5px 10px',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 40,
    '&$expanded': {
      minHeight: 40,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails);



export default function ScheduledRoadTestAccordion() {
  const [expanded, setExpanded] = React.useState('panel1');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const [clicked, isClicked] = React.useState(false)

  const handleClick = () =>{
      isClicked(!clicked)
  }

  return (
    <div>
      <Accordion className='accordionLesson' square onChange={handleChange('panel1')} onClick={handleClick}>
        <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
            <DateRangeIcon/>&nbsp;<Typography className='accordionHeaderText'>Click Here to View Scheduled Lessons</Typography>
            {
                clicked ?<RemoveIcon style={{marginLeft: 'auto', fontWeight: '100'}}/>:<AddIcon style={{marginLeft: 'auto', fontWeight: '100'}}/>
            }
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
