import React from 'react';
import { Calendar } from 'primereact/calendar';
import {Stop} from '@material-ui/icons';
import Typography from '@material-ui/core/Typography';

export default function RoadTestCalendar() {
  
    const [date, setDate] = React.useState('')

  return (
    <>
        <Calendar inline value={date} onChange={(e) => setDate(e.value)} numberOfMonths={2} />
        <div style={{marginTop: '10px'}}>
          <Stop style={{fill: '#2abc79', fontSize: '1rem', marginRight: '5px', backgroundColor: '#2abc79'}}/>
          <Typography style={{fontSize: '13px', color: '#2abc79', display: 'inline'}}>Available</Typography>
          <Stop style={{fill: '#fbb9b5', fontSize: '1rem', marginRight: '5px', marginLeft: '10px', backgroundColor: '#fbb9b5'}}/>
          <Typography style={{fontSize: '13px', display: 'inline'}}>Booked</Typography>
          <Stop style={{fill: '#e8e8e8', fontSize: '1rem', marginRight: '5px', marginLeft: '10px', backgroundColor: '#e8e8e8'}}/>
          <Typography style={{fontSize: '13px', display: 'inline'}}>Date unavailable or slots are not created</Typography>
        </div>
    </>
  );
}