import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
// import {Table, Button, TableBody, TablePagination, TableCell, TableContainer, TableHead, TableRow, Paper} from '@material-ui/core';
import Table from '@material-ui/core/Table'
import Button from '@material-ui/core/Button'
import TableBody from '@material-ui/core/TableBody'
import TablePagination from '@material-ui/core/TablePagination'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.white,
    fontWeight: '700'
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(date, name, location) {
  return { date, name, location };
}

const columns = [
    { id: 'date', label: 'Date', minWidth: 170 },
    { id: 'name', label: 'Location', minWidth: 170 },
    { id: 'location', label: 'Pickup Location', minWidth: 170 },
  ];

const rows = [
  createData(<Button>mon, feb 15, 8:45 am - 9:15 am</Button>, 'Avon The Next Street', 'Avon TNS'),
  createData(<Button>mon, feb 15, 8:45 am - 9:15 am</Button>, 'Avon The Next Street', 'Avon TNS'),
  createData(<Button>mon, feb 15, 8:45 am - 9:15 am</Button>, 'Avon The Next Street', 'Avon TNS'),
  createData(<Button>mon, feb 15, 8:45 am - 9:15 am</Button>, 'Avon The Next Street', 'Avon TNS'),
  createData(<Button>mon, feb 15, 8:45 am - 9:15 am</Button>, 'Avon The Next Street', 'Avon TNS'),
  createData(<Button>mon, feb 15, 8:45 am - 9:15 am</Button>, 'Avon The Next Street', 'Avon TNS'),
  createData(<Button>mon, feb 15, 8:45 am - 9:15 am</Button>, 'Avon The Next Street', 'Avon TNS')
];

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function RoadTestTable() {
  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
      <>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow style={{color: '#2abc79 !important'}}>
              {columns.map((column) => (
                <StyledTableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </StyledTableCell>
              ))}
            </TableRow>
        </TableHead>
        <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
              return (
                <StyledTableRow hover role="checkbox" tabIndex={-1} key={row.index}>
                  {columns.map((column, index) => {
                    const value = row[column.id];
                    return (
                      <StyledTableCell key={index} align={column.align}  className='tableDataButton'>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </StyledTableCell>
                    );
                  })}
                </StyledTableRow>
              );
            })}
          </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
    rowsPerPageOptions={[10,20,30,40,50]}
    component="div"
    count={rows.length}
    rowsPerPage={rowsPerPage}
    page={page}
    onChangePage={handleChangePage}
    onChangeRowsPerPage={handleChangeRowsPerPage}
  />
  </>
  );
}