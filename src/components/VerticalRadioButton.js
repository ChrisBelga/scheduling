import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';


const icon = <RadioButtonUncheckedIcon fontSize="small" />;
const checkedIcon = <CheckCircleIcon fontSize="small" />;

export default function VerticalRadioButton() {
  const [value, setValue] = React.useState('anytime');

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <FormControl component="fieldset">
      <FormLabel className='scheduleRadioButtons' component="legend">Appointment Start Time</FormLabel>
      <RadioGroup aria-label="appointment" name="gender1" value={value} onChange={handleChange}>
        <FormControlLabel value="anytime" control={<Radio checkedIcon={checkedIcon} icon={icon} />} label="Anytime" />
        <FormControlLabel value="beforenoon" control={<Radio checkedIcon={checkedIcon} icon={icon} />} label="Start Before Noon" />
        <FormControlLabel value="afternoon" control={<Radio checkedIcon={checkedIcon} icon={icon} />} label="Start After Noon" />
      </RadioGroup>
    </FormControl>
  );
}
