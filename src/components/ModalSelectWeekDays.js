/* eslint-disable no-use-before-define */

import React from 'react';
// import {Checkbox, TextField} from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export default function ModalSelectWeekDays() {

  const [selectedWeekdays, setSelectedWeekdays] = React.useState([])
  console.log({selectedStatus})

  return (
    <Autocomplete
      multiple
      id="week-days-checkboxes"
      options={weekdays}
      disableCloseOnSelect
      size='small'
      fullWidth={true}
      onChange={(e, status) => {
        setSelectedWeekdays(status);
      }}
      getOptionLabel={(option) => option.title}
      renderOption={(option, state) => {
        const selectWeekdays = selectedWeekdays.findIndex(
          days => days.title.toLowerCase() === "select all"
        );
        if (selectWeekdays > -1) {
          state.selected = true;
        }
        return (
        <React.Fragment>
          <Checkbox
            icon={icon}
            checkedIcon={checkedIcon}
            style={{ marginRight: 8 }}
            checked={state.selected}
          />
          {option.title}
        </React.Fragment>
        )
      }}
      renderInput={(params) => (
        <TextField {...params} variant="outlined" label="Select Week Days" />
      )}
    />
  );
}

const weekdays = [
  { title: 'Select All' },
  { title: 'Monday'},
  { title: 'Tuesday'},
  { title: 'Wednesday'},
  { title: 'thursday'},
  { title: 'Friday'},
  { title: 'Saturday'},
  { title: 'Sunday'}
];