import React from 'react'
import SearchIcon from '@material-ui/icons/Search';
import VerticalRadioButton from './VerticalRadioButton'
import {Modal, Button} from 'react-bootstrap'
import ModalSelectWeekDays from './ModalSelectWeekDays';

export default function RefineSearchModal(props) {
    
    return (
        <>
            <Modal {...props} >
                <Modal.Header closeButton>
                <Modal.Title style={{fontSize: '17px'}}><SearchIcon/>Refine Search</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <VerticalRadioButton/>
                    <ModalSelectWeekDays/>
                </Modal.Body>
                <Modal.Footer>
                <Button 
                variant="secondary" 
                onClick={props.onHide}
                >
                    SEARCH
                </Button>
                <Button 
                variant="primary" 
                onClick={props.onHide}
                >
                    CLOSE
                </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}
