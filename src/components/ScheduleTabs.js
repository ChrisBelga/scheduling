import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
// import {AppBar, Tabs, Tab, Box} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'
// import MySchedule from './MySchedule/MySchedule';
import ScheduleMyLessons from './ScheduleMyLessons/ScheduleMyLessons';
import ScheduleRoadTest from './ScheduleRoadTest/ScheduleRoadTest';
import DateRangeIcon from '@material-ui/icons/DateRange';
import SpeedIcon from '@material-ui/icons/Speed';
import DirectionsCarIcon from '@material-ui/icons/DirectionsCar';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

export default function ScheduleTabs() {
  const [value, setValue] = React.useState(0);
  let link = window.location.href;

  useEffect(() => {
    var url =new URL(link);
    var id = url.searchParams.get("scheduling");
    if(id !== ""){
      if(id <= 2){
        a11yProps(id);
        var ID = parseFloat(id);
        setValue(ID)
      }else{
        var initialID = 0;
        a11yProps(initialID);
        setValue(initialID);
      }
    }
    
  }, [link])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleParams = (id) => {
    var state = {};
    let url = new URL(link);
    url.searchParams.set("scheduling", id);
    window.history.pushState(state, "Page Title" , url.toString())
  }

  return (
    <div className='schedulingTabs'>
      <AppBar position="static">
        <Tabs
          // variant="fullWidth"
          value={value}
          onChange={handleChange}
          aria-label="nav tabs example"
        >
          <LinkTab icon={<DateRangeIcon/>} className='tab1' label="MY SCHEDULE" {...a11yProps(0)} onClick={() => handleParams(0)} />
          <LinkTab icon={<SpeedIcon/>} className='tab2' label="SCHEDULE MY LESSONS" {...a11yProps(1)} onClick={() => handleParams(1)} />
          <LinkTab icon={<DirectionsCarIcon/>} className='tab3' label="SCHEDULE MY ROAD TEST" {...a11yProps(2)} onClick={() => handleParams(2)} />
        </Tabs>
      </AppBar>


      <TabPanel value={value} index={0}>
        {/* <MySchedule/> */}
      </TabPanel>

      <TabPanel className='scheduledLessons' value={value} index={1}>
        <ScheduleMyLessons/>
      </TabPanel>

      <TabPanel className='scheduledRoadTest' value={value} index={2}>
        <ScheduleRoadTest/>
      </TabPanel>


    </div>
  );
}