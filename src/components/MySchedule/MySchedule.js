import React, { useState, Fragment } from "react";
import {
  Row,
  Col,
  ToggleButton,
  ToggleButtonGroup,
  Button as BootstrapButton,
  Modal,
} from "react-bootstrap";
import { Calendar } from "primereact/calendar";
import { Typography, useMediaQuery, useTheme } from "@material-ui/core";
import DatePicker from "react-horizontal-datepicker";


export default function MySchedule(props) {
  const theme = useTheme();
  const xsSize = useMediaQuery(theme.breakpoints.down('xs'));

  const [filterValue, setFilterValue] = useState([]);

  const handleFilterChange = (val) => {
    setFilterValue(val);
  };

  const [showCalendar, setShowCalendar] = useState(false);

  const handleOpenCalendar = () => setShowCalendar(true);
  const handleCloseCalendar = () => setShowCalendar(false);

  //const [datePicked, setDatePicked] = useState(0);

  const selectedDay = (val) => {
    console.log(val)
  };

  return (
    <Fragment>
      <Row>
        <Col lg={6} md={12} sm={12} xs={12}>
          <Row>
            <Col>
              <div className="schedule-datepicker-container">
                <DatePicker
                  getSelectedDay={selectedDay}
                  labelFormat={"MMMM"}
                  color={"#3598dc"}
                  endDate={41}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="filter-toggle-button-container">
                <ToggleButtonGroup
                  vertical={xsSize}
                  type="checkbox"
                  value={filterValue}
                  onChange={handleFilterChange}
                  id="filter-toggle-button-group"
                >
                  <ToggleButton id="filter-toggle-button" value="classes">
                    CLASSES
                  </ToggleButton>
                  <ToggleButton id="filter-toggle-button" value="in-car">
                    IN-CAR
                  </ToggleButton>
                  <ToggleButton id="filter-toggle-button" value="testing">
                    TESTING
                  </ToggleButton>
                  <BootstrapButton
                    id="filter-calendar-button"
                    onClick={handleOpenCalendar}
                  >
                    SHOW CALENDAR
                  </BootstrapButton>
                </ToggleButtonGroup>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="appointments-container">
                <Typography>No appointments found.</Typography>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <Modal centered show={showCalendar} onHide={handleCloseCalendar} id="show-calendar-modal">
        <Modal.Header closeButton>
          <Modal.Title>
            <Typography>
              Select Date
            </Typography>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Calendar inline numberOfMonths={2}></Calendar>
        </Modal.Body>
        <Modal.Footer>
          <BootstrapButton variant="primary" onClick={handleCloseCalendar} id="calendar-close-btn">
            Close
          </BootstrapButton>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}
