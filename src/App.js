import React from 'react';
import './App.scss';
import {Row, Col} from 'react-bootstrap'
import Scheduling from './components/Scheduling/Scheduling';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'bootstrap/dist/css/bootstrap.min.css'

export default function App({ moduleData }) {
  // eslint-disable-next-line no-console
  return (
    <>
    <Row>
      <Col className='myScheduleCol'>
        <Scheduling /> 
      </Col>
    </Row>
  </>
  );
}
